<?php 
    
    class DebugSoapClient extends SoapClient {
      public $sendRequest = true;
      public $printRequest = false;
      public $formatXML = false;

      public function __doRequest($request, $location, $action, $version, $one_way=0) {
        if ( $this->printRequest ) {
          if ( !$this->formatXML ) {
            $out = $request;
          }
          else {
            $doc = new DOMDocument;
            $doc->preserveWhiteSpace = false;
            $doc->loadxml($request);
            $doc->formatOutput = true;
            $out = $doc->savexml();
          }
          echo $out;
        }

        if ( $this->sendRequest ) {
          return parent::__doRequest($request, $location, $action, $version, $one_way);
        }
        else {
          return '';
        }
      }
    }

    
	$client = new DebugSoapClient('https://netconnect.bluedart.com/Ver1.8/ShippingAPI/Finder/ServiceFinderQuery.svc?wsdl', array('soap_version' => SOAP_1_2));
	$ap_param = array(
                    'pinCode'     =>    '400097',
                    'profile'	=> [
                    	'LoginID' => 'BLR02225',
                    	'LicenceKey' => '119bac75593f1247c7e6f70d13b090ba'
                    ]
                );

	try {
        $info = $client->__call("GetServicesforPincode", array($ap_param));
        print_r($info);
    } 
    catch (SoapFault $fault) {
        print_r($fault->faultstring);
    }

	// $res    = $client->ItemQuery('screwdriver');
	// $count  = $res->count;
	// $size   = $res->size;
	// print "count=$count, size=$size\n";
?>